using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{
    /// <summary>
    /// Tiempo de recarga en segundos
    /// </summary>
    public float tiempoRecarga = 1;

    /// <summary>
    /// Tiempo de recarga en segundos
    /// </summary>
    public bool sePuedeDisparar = true;

    /// <summary>
    /// Municion restante del arma
    /// </summary>
    public int municion = 0;

    /// <summary>
    /// Damage del arma
    /// </summary>
    public int damage = 0;

    /// <summary>
    /// Velocidad de la bala
    /// </summary>
    public int velocidad = 0;

    /// <summary>
    /// Bala
    /// </summary>
    public GameObject prefabBala;

    /// <summary>
    /// Salida del disparo, se dispara desde el forward
    /// </summary>
    public Transform centroTiro;

    /// <summary>
    /// AudioSource del arma
    /// </summary>
    public AudioSource audioSource;

    /// <summary>
    /// Indica si es un enemigo quien porta el arma
    /// </summary>
    public bool isEnemigo = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!isEnemigo)
        GestorUI.actualizarMunicion(municion);
    }

    /// <summary>
    /// Dispara una bala
    /// </summary>
    public void disparar(Vector3 rotaciones, GameObject origen, Objeto objeto = null)
    {
        if (sePuedeDisparar && objeto != null && objeto.isAccionador)
        {
            this.sePuedeDisparar = false;
            objeto.quienLoAcciona = origen;
            objeto.realizarAccion();
            Invoke("reiniciarDisparo", 1.4f);
        }
        else if (sePuedeDisparar && municion > 0)
        {
            this.sePuedeDisparar = false;
            Vector2 rotaciones2 = this.prefabBala.transform.rotation.eulerAngles;
            Quaternion rotacion = Quaternion.Euler(new Vector3(rotaciones2.x,rotaciones.y, rotaciones.z));
            Vector3 posicion = this.centroTiro.transform.position;
            GameObject bala = Instantiate(prefabBala, posicion, rotacion);
            bala.GetComponent<Bala>().velocidad = this.velocidad;
            bala.GetComponent<Bala>().damage = this.damage;
            bala.GetComponent<Bala>().origin = origen;

            Invoke("reiniciarDisparo", tiempoRecarga);
            audioSource.Play();
   
            if(!isEnemigo)
            municion--;
        }
    }

    /// <summary>
    /// indica que ya se puede volver a disparar
    /// </summary>
    private void reiniciarDisparo()
    {
        sePuedeDisparar = true;
    }
}
