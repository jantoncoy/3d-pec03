using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    /// <summary>
    /// Indica si colisiono la bala
    /// </summary>
    private bool colisiono;

    /// <summary>
    /// Rigid de la bala
    /// </summary>
    private Rigidbody rigid;

    /// <summary>
    /// Damage del arma
    /// </summary>
    public int damage = 0;

    /// <summary>
    /// Velocidad de la bala
    /// </summary>
    public float velocidad;

    /// <summary>
    /// Se oye disparo y se destruye
    /// </summary>
    public AudioClip explosion;

    /// <summary>
    /// AudioSource de la bala
    /// </summary>
    public AudioSource audioSource;

    /// <summary>
    /// Pasado este tiempo la bala es destruida
    /// </summary>
    public float tiempoDeVida = 2.6f;

    /// <summary>
    /// Quien disparo a la bala
    /// </summary>
    public GameObject origin; 

    // Start is called before the first frame update
    void Start()
    {
        colisiono = false;
        rigid = this.GetComponent<Rigidbody>();

        //Pasado el tiempo de la bala se destruye
        Invoke("destruirBala", tiempoDeVida);
    }

    // Update is called once per frame
    void Update()
    {
        if (!colisiono)
        {
            //La bala avanza
            avanzar();
        }
        else
        {
            //Si dejo de reproducirse la explosion se elimina la bala
            if (!audioSource.isPlaying)
            {
                destruirBala();
            }
        }
    }

    /// <summary>
    /// Destruye la bala
    /// </summary>
    private void destruirBala()
    {
        Destroy(this.gameObject);
    }

    /// <summary>
    /// Avanza la bala
    /// </summary>
    private void avanzar()
    {
        Vector3 velocidad = this.gameObject.transform.up * this.velocidad;
        this.rigid.velocity = velocidad;
    }

    /// <summary>
    /// Gestiona cuando la bala colisiona
    /// </summary>
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.name.Contains("Bala") 
            && !collision.gameObject.name.Contains("Arma")
            && collision.gameObject != origin)
        {
            colisiona();
        }
    }

    /// <summary>
    /// Indica que hacer cuando la bala colisiona
    /// </summary>
    private void colisiona()
    {
        colisiono = true;
        //Se pone velocidad a cero
        this.rigid.velocity = Vector3.zero;
        Debug.Log("Colisiona la bala");
        //Se para audio
        audioSource.Stop();
        //Se agrega audio de explosion o impacto
        audioSource.clip = explosion;
        //Se reproduce sonido
        audioSource.Play();
    }

}
