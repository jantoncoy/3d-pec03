using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Objeto : MonoBehaviour
{
    /// <summary>
    /// Cantidad de unidades que recupera o da el item
    /// </summary>
    public int cantidad = 0;

    /// <summary>
    /// Objeto de tipo municion
    /// </summary>
    public bool isMunicion = false;
    
    /// <summary>
    /// Objeto de tipo vida
    /// </summary>
    public bool isVida = false;
    
    /// <summary>
    /// Objecto de tipo escudo
    /// </summary>
    public bool isEscudo = false;

    /// <summary>
    /// Objeto que sirve para accionar algo
    /// </summary>
    public bool isAccionador = false;

    /// <summary>
    /// Objeto que sirve para accionar algo
    /// </summary>
    public bool isPlataforma = false;

    /// <summary>
    /// Indica si se puede recolectar
    /// </summary>
    public bool isRecolectable = false;

    /// <summary>
    /// Indica el objeto que acciona (puerta, enemigos, etc)
    /// </summary>
    public Objeto queAcciona = null;

    /// <summary>
    /// Quien acciona el objeto
    /// </summary>
    public GameObject quienLoAcciona = null;

    /// <summary>
    /// Sonido que suena cuando se activa o se recolecta
    /// </summary>
    private AudioSource sonidoAccion;

    /// <summary>
    /// Indica si se acciono el objeto
    /// </summary>
    public bool seAcciono = false;

    /// <summary>
    /// Movimiento
    /// </summary>
    private int movimiento = 5;

    /// <summary>
    /// Indica si sirve para cambiar de zona
    /// </summary>
    public string isCambiarZona = null;

    /// <summary>
    /// Tiene la cantidad de movimiento que se le aplica a la plataforma en el fixed update anterior
    /// </summary>
    private float ultimoMovimiento = 0;

    /// <summary>
    /// Indica si es el final del nivel
    /// </summary>
    public bool isFinalNivel = false;

    // Start is called before the first frame update
    void Start()
    {
        //Recogemos la instancia de recoleccion
        sonidoAccion = GetComponentInChildren<AudioSource>();
    }

    /// <summary>
    /// Reproduce el sonido del objeto
    /// </summary>
    private void playSonido()
    {
        sonidoAccion.Play();
    }

    /// <summary>
    /// Detectamos colision con entidad
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Colisiona objeto");
        quienLoAcciona = collision.gameObject;
        realizarAccion();
    }

    /// <summary>
    /// Detectamos lanzamiento de evento
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Lanzo trigger objeto");
        quienLoAcciona = other.gameObject;
        realizarAccion();
    }

    /// <summary>
    /// Esta parte transfiere el movimiento de la plataforma al jugador
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        //Si esta encima aplicamos el ultimo movimiento de la plataforma
        if (isPlataforma && other.gameObject.CompareTag("Player") && ultimoMovimiento != 0)
        {
            CharacterController personaje = other.gameObject.GetComponent<CharacterController>();

            if (personaje != null)
            personaje.Move(new Vector3(0, 0, ultimoMovimiento));

            ultimoMovimiento = 0;
        }
    }

    /// <summary>
    /// Se llama cuando es tocado o salta evento
    /// </summary>
    public void realizarAccion()
    {
        Jugador jugador = quienLoAcciona.GetComponent<Jugador>();
        if (isRecolectable && jugador != null)
        {
            playSonido();
            seAcciono = true;
        }else if(isPlataforma && jugador != null)
        {
            if (quienLoAcciona.name.Contains("Limite"))
            {
                if(movimiento > 0)
                {
                    movimiento = -5;
                }
                else
                {
                    movimiento = 5;
                }
            }
        }else if (isAccionador && jugador != null)
        {
            this.seAcciono = !this.seAcciono;

            if (isFinalNivel)
            {
                //Si es final activamos panel de victoria
                GestorUI.activarPanelVictoria();
            }
            else if(isCambiarZona != null && !isCambiarZona.Equals("") && jugador != null)
            {
                //Guardamos la zona a la que vamos
                Menu.zonaActual = isCambiarZona;
                //Nos cambiamos de zona
                GestorUI.activarPanelCargando(isCambiarZona);
            }
            else if(jugador != null)
            {
                //Cambiamos el estado del objeto que acciona
                queAcciona.seAcciono = !queAcciona.seAcciono;
            }
        }
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(isRecolectable && seAcciono && !sonidoAccion.isPlaying)
        {
            //Se destruye si termino de reproducirse el sonido
            Destroy(this.gameObject);
        }else if(isPlataforma && seAcciono)
        {
            ultimoMovimiento = movimiento * Time.deltaTime;
            this.gameObject.transform.Translate(0,0, ultimoMovimiento);
        }
    }
}
