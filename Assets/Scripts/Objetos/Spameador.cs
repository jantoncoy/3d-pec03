using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase que crear instancias de un objeto pasado, conforme pasa el tiempo genera hasta el maximo que se le indique
/// </summary>
public class Spameador : MonoBehaviour
{
    /// <summary>
    /// Entidades que genera inicialmente
    /// </summary>
    public int cantidadInicial = 1;

    /// <summary>
    /// Entidades maximos que genera
    /// </summary>
    public int cantidadMaximo = 10;

    /// <summary>
    /// Cada tiempoIncremento aumenta la cantidadInicial hasta cantidadMaxima(en segundos)
    /// </summary>
    public int tiempoIncremento = 30;

    /// <summary>
    /// Cada tiempoSpameo crear las entidades (en segundos)
    /// </summary>
    public int tiempoSpameo = 20;

    /// <summary>
    /// Indica si se enecuentra activo la creacion de entidades
    /// </summary>
    public bool activo = true;

    /// <summary>
    /// Entidad que genera
    /// </summary>
    public GameObject entidad;

    /// <summary>
    /// Lista de puntos que recorrera la entidad
    /// </summary>
    public List<Transform> waypoints;

    /// <summary>
    /// Inicializa los arrays
    /// </summary>
    private void Start()
    {
        InvokeRepeating(nameof(crearMonstruos), 1, tiempoSpameo);
        InvokeRepeating(nameof(aumentarCantidad), tiempoIncremento, tiempoIncremento);
    }

    /// <summary>
    /// Aumenta la cantidad inicial en un 20% de la cantidad maxima
    /// </summary>
    private void aumentarCantidad()
    {
        if (activo)
        {
            int nuevaCantidad = (int)(cantidadInicial + ((float)cantidadMaximo * 0.2f));

            if(nuevaCantidad > cantidadMaximo)
            {
                cantidadInicial = cantidadMaximo;
            }
            else
            {
                cantidadInicial = nuevaCantidad;
            }
        }
    }

    /// <summary>
    /// Crea tantos monstruos como la cantidad inicial
    /// </summary>
    private void crearMonstruos()
    {
        if (activo)
        {
            GameObject nuevaEntidad = Instantiate(entidad, this.transform.position, this.transform.rotation);
            
            Enemigo enemigo = entidad.GetComponent<Enemigo>();
            
            if(enemigo != null)
            enemigo.waypoints = this.waypoints;
        }
    }
}
