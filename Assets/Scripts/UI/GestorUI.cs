using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GestorUI : MonoBehaviour
{
    /// <summary>
    /// Evento que sirve para actualizar la vida
    /// </summary>
    public static Action<int> actualizarVida;

    /// <summary>
    /// Evento que sirve para actualizar el escudo
    /// </summary>
    public static Action<int> actualizarEscudo;
    
    /// <summary>
    /// Evento que sirve para actualizar la municion
    /// </summary>
    public static Action<int> actualizarMunicion;
    
    /// <summary>
    /// Evento que sirve para actualizar el icono del arma
    /// </summary>
    public static Action<int> actualizarArma;

    /// <summary>
    /// Activa los paneles del hud
    /// </summary>
    public static Action activarPanelGameOver, activarPanelVictoria;
    /// <summary>
    /// Activa el panel cargando
    /// </summary>
    public static Action<string> activarPanelCargando;

    /// <summary>
    /// Evento que sirve para actualizar el icono del arma
    /// </summary>
    public static Action<String,float> mostrarMensaje;

    /// <summary>
    /// Texto de la municion
    /// </summary>
    public TextMeshProUGUI municion, mensaje;

    /// <summary>
    /// Imagenes de las armas
    /// </summary>
    public GameObject arma1, arma2, mensajeObject;

    /// <summary>
    /// Barra de vida y escudo
    /// </summary>
    public Image barraVida, barraEscudo;

    /// <summary>
    /// Paneles de gameover y victoria
    /// </summary>
    public GameObject panelGameOver,panelVictoria,panelCargando;

    // Start is called before the first frame update
    void Start()
    {
        GestorUI.actualizarArma = updateArma;
        GestorUI.actualizarMunicion = updateMunicion;
        GestorUI.actualizarVida = updateVida;
        GestorUI.actualizarEscudo = updateEscudo;
        GestorUI.mostrarMensaje = mostrarMensajeEmergente;

        GestorUI.activarPanelCargando = (string nombreScene) => {
            this.panelCargando.SetActive(true);
            SceneManager.LoadScene(nombreScene);
        };

        GestorUI.activarPanelVictoria = () => {
            this.panelVictoria.SetActive(true);
        };

        GestorUI.activarPanelGameOver = () => {
            this.panelGameOver.SetActive(true);
        };
    }

    /// <summary>
    /// Redimensiona la barra de vida
    /// </summary>
    /// <param name="value"></param>
    private void updateVida(int value)
    {
        barraVida.fillAmount = 0.01f * value;
    }

    /// <summary>
    /// Redimensiona la barra del escudo
    /// </summary>
    /// <param name="value"></param>
    private void updateEscudo(int value)
    {
        barraEscudo.fillAmount = 0.01f * value;
    }

    /// <summary>
    /// Cambia el icono del arma
    /// </summary>
    /// <param name="value"></param>
    private void updateArma(int value)
    {
        if(value == 0)
        {
            //Arma principal potente
            arma1.SetActive(true);
            arma2.SetActive(false);
        }
        else if(value == 1)
        {
            //Arma secundaria ligera
            arma1.SetActive(false);
            arma2.SetActive(true);
        }
    }

    /// <summary>
    /// Actualiza la cantidad de municion
    /// </summary>
    /// <param name="value"></param>
    private void updateMunicion(int value)
    {
        municion.text = value+"";
    }

    /// <summary>
    /// Muestra un mensaje durante el tiempo pasado por parametro
    /// </summary>
    /// <param name="mensaje"></param>
    /// <param name="tiempoSegundos"></param>
    private void mostrarMensajeEmergente(String mensaje, float tiempoSegundos)
    {
        CancelInvoke("ocultarMensaje");
        mensajeObject.SetActive(true);
        this.mensaje.text = mensaje;
        Invoke("ocultarMensaje", tiempoSegundos);
    }

    /// <summary>
    /// Oculta el mensaje
    /// </summary>
    private void ocultarMensaje()
    {
        mensaje.text = "";
        mensajeObject.SetActive(false);
    }
}
