using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Contiene las acciones para los botones y menus
/// </summary>
public class Menu : MonoBehaviour
{
    public static string zonaActual = "Zona1_1";

    /// <summary>
    /// accion cuando se pulsa en iniciar
    /// </summary>
    public void iniciar()
    {
        zonaActual = "Zona1_1";
        SceneManager.LoadScene("Zona1_1");
    }

    /// <summary>
    /// Vuelve al menu de inicio
    /// </summary>
    public void menu()
    {
        SceneManager.LoadScene("Menu");
    }

    /// <summary>
    /// accion cuando se pulsa en salir
    /// </summary>
    public void salir()
    {
        Application.Quit(1);
    }

    /// <summary>
    /// Reinicia la zona actual
    /// </summary>
    public void Reiniciar()
    {
        SceneManager.LoadScene(Menu.zonaActual);
    }
}
