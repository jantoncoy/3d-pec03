using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemigo : MonoBehaviour
{
    /// <summary>
    /// Estado que ejecuta la IA
    /// </summary>
    public Estado estado;

    /// <summary>
    /// Estado de ataque
    /// </summary>
    public EstadoAtaque estadoAtaque;

    /// <summary>
    /// Estado de perseguir
    /// </summary>
    public EstadoPerseguir estadoPerseguir;

    /// <summary>
    /// Estado de vigilar
    /// </summary>
    public EstadoVigilar estadoVigilar;

    /// <summary>
    /// Estado de ver
    /// </summary>
    public EstadoVer estadoVer;

    /// <summary>
    /// Gameobject del jugador
    /// </summary>
    public GameObject jugador;

    /// <summary>
    /// Lista de posiciones que debe seguir el enemigo
    /// </summary>
    public List<Transform> waypoints;

    /// <summary>
    /// WayPoint actual
    /// </summary>
    public int waypointsActual;

    /// <summary>
    /// Vida del enemigo
    /// </summary>
    public int vida;

    /// <summary>
    /// Arma actual
    /// </summary>
    public GameObject arma;

    /// <summary>
    /// Cuando se debe subir la vista del raytracing
    /// </summary>
    public float subirVista = 0;

    /// <summary>
    /// Navmeshagent del agente
    /// </summary>
    private NavMeshAgent navmeshagent;

    /// <summary>
    /// gestor de animaciones
    /// </summary>
    private Animator animator;

    /// <summary>
    /// Emisor de audio del enemigo
    /// </summary>
    public AudioSource audio;


    /// <summary>
    /// Indica cada cuanto procesar la IA en MS
    /// </summary>
    float procesar = 1000.0f / 15.0f;

    /// <summary>
    /// Acumulacion de tiempo desde la ultima ejecucion en MS
    /// </summary>
    float acumulado = 0;

    /// <summary>
    /// Indica si esta vivo
    /// </summary>
    bool estaVivo = true;

    /// <summary>
    /// Distancia a la que perseguir el enemigo
    /// </summary>
    public int distanciaPerseguir = 40;

    /// <summary>
    /// Distancia a la que se queda el enemigo para atacar
    /// </summary>
    public int distanciaAtaque = 1;

    /// <summary>
    /// Transparencia que se aplica cuando muere
    /// </summary>
    private float transparencia = 1.0f;

    /// <summary>
    /// Modelo del enemigo
    /// </summary>
    public SkinnedMeshRenderer modelo;

    /// <summary>
    /// Item que soltara el enemigo
    /// </summary>
    public GameObject itemSoltar;

    /// <summary>
    /// Damage de un golge cuerpo a cuerpo
    /// </summary>
    public int ataqueCuerpo = 20;

    /// <summary>
    /// Audios del enemigo
    /// </summary>
    public AudioClip attack, normal;

    /// <summary>
    /// Inicializamos variables
    /// </summary>
    void Start()
    {
        this.estadoAtaque = new EstadoAtaque(this.gameObject);
        this.estadoPerseguir = new EstadoPerseguir(this.gameObject);
        this.estadoVer = new EstadoVer(this.gameObject);
        this.estadoVigilar = new EstadoVigilar(this.gameObject);

        estado = this.estadoVer;
        navmeshagent = this.GetComponent<NavMeshAgent>();
        animator = this.GetComponent<Animator>();
        audio = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (estaVivo)
        {
            if (acumulado > procesar)
            {
                //Procesamos si tiene vida
                if (vida > 0)
                    estado.procesar();

                acumulado = 0.0f;
            }
            else
            {
                //Sumamos tiempo
                acumulado += Time.deltaTime / 0.001f;
            }

            if (!navmeshagent.isStopped)
            {
                //desactivamos arma
                if(arma != null)
                    arma.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Cuando impacta un objeto salta este evento
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        Bala bala = collision.gameObject.GetComponent<Bala>();
        Jugador jugador = collision.gameObject.GetComponent<Jugador>();
        if (bala != null)
        {
            //Animacion dolor
            animator.SetInteger("Estado", 4);

            //quitan puntos de vida segun el damage interno
            if (bala.origin.GetComponent<Enemigo>() == null)
            {
                //Le quitamos vida
                quitarVida(bala.damage);

                //Le asignamos la posicion del jugador
                this.jugador = bala.origin;

                //Cambiamos a perseguir
                estado = estadoPerseguir;
            }
        }
        else if(jugador != null && estado.Equals(estadoAtaque))
        {
            //Hacemos da�o
            jugador.quitarVida(ataqueCuerpo);
        }

        Debug.Log("Enemigo colisiona " + collision.gameObject.name);
    }

    /// <summary>
    /// Quita vida
    /// </summary>
    /// <param name="damage"></param>
    private void quitarVida(int damage)
    {
        int resultado = vida - damage;
        
        if(resultado < 0)
        {
            resultado = 0;
        }

        vida = resultado;

        if(vida <= 0)
        {
            //muere
            animator.SetInteger("Estado", 2);
            navmeshagent.isStopped = true;
            if(arma != null)
                this.arma.SetActive(false);
            estaVivo = false;//deshabilitamos el procesamiento de su IA
            morir();
        }
    }

    /// <summary>
    /// Llama a aplicar el fade cada cierto tiempo
    /// </summary>
    private void morir()
    {
        InvokeRepeating(nameof(aplicarTransparencia),0.10f,0.10f);
    }

    /// <summary>
    /// Aplicara una transparencia cada cierto tiempo
    /// </summary>
    private void aplicarTransparencia()
    {
        if(transparencia >= 0.00f)
        {
            Debug.Log("Trans" + transparencia);
            transparencia -= 0.01f;
            fade(transparencia);
        }
        else
        {
            CancelInvoke();
            soltarItem();
            Destroy(this.gameObject);
        }
    }

    /// <summary>
    /// Soltara un item si sale el numero correto
    /// </summary>
    private void soltarItem()
    {
        //30% de posibilidad de que suelte item
        if (itemSoltar != null && Random.Range(1,11) > 7)
        {
            Instantiate(itemSoltar, this.gameObject.transform.position, this.transform.rotation);
        }
        
    }

    /// <summary>
    /// desaparece al enemigo
    /// </summary>
    public void fade(float transparencia)
    {
        if(this.modelo != null)
        {
            Material[] materiales = this.modelo.materials;

            if (materiales != null && materiales.Length > 0)
                foreach (Material mat in materiales)
                {
                    if (mat != null)
                    {
                        mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                        mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                        mat.SetInt("_ZWrite", 0);
                        mat.DisableKeyword("_ALPHATEST_ON");
                        mat.DisableKeyword("_ALPHABLEND_ON");
                        mat.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                        mat.renderQueue = 3000;
                        mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, transparencia);
                        Debug.Log(transparencia + " " + mat.name);
                    }
                }
        }

    }

    /// <summary>
    /// Pone el audio de ataque
    /// </summary>
    public void ponerAtaque()
    {
        if (audio != null && attack != null)
        {
            audio.clip = attack;
            audio.Play();
        }
            
    }

    /// <summary>
    /// Pone el audio de estado normal
    /// </summary>
    public void ponerNormal()
    {
        if (audio != null && normal != null && audio.clip != normal)
        {
            audio.clip = normal;
            audio.loop = true;
            audio.Play();
        }

    }
}
