﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EstadoVigilar : Estado
{
    /// <summary>
    /// hace referencia al gameobject del enemigo
    /// </summary>
    private GameObject my;

    /// <summary>
    /// hace referencia al script enemigo del enemigo
    /// </summary>
    private Enemigo parent;

    /// <summary>
    /// hace referencia al componente navmeshagent del enemigo
    /// </summary>
    private NavMeshAgent navmeshagent;

    /// <summary>
    /// Constructor del estado vigilar
    /// </summary>
    public EstadoVigilar(GameObject my)
    {
        this.my = my;
        parent = my.GetComponent<Enemigo>();
        navmeshagent = my.GetComponent<NavMeshAgent>();
    }

    /// <summary>
    /// cambia al estado ataque
    /// </summary>
    public void cambiarAtaque()
    {
        parent.estado = parent.estadoAtaque;
    }

    /// <summary>
    /// cambia al estado perseguir
    /// </summary>
    public void cambiarPerseguir()
    {
        parent.estado = parent.estadoPerseguir;
    }

    /// <summary>
    /// cambia al estado ver
    /// </summary>
    public void cambiarVer()
    {
        parent.estado = parent.estadoVer;
    }

    /// <summary>
    /// cambia al estado vigilar
    /// </summary>
    public void cambiarVigilar()
    {
        Debug.Log("Ya se encuentra en el estado");
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerEnter(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerExit(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerStay(Collider col)
    {
        throw new System.NotImplementedException();
    }
    
    /// <summary>
    /// procesa el estado
    /// </summary>
    public void procesar()
    {
        if(parent.waypoints == null || parent.waypoints.Count == 0)
        {
            return;
        }

        //se inicializa al primero waypoint
        if(parent.waypointsActual < 0)
        {
            parent.waypointsActual = 0;
        }

        //Obtenemos todas las posiciones
        Transform[] posiciones = parent.waypoints.ToArray();
        int posicion = parent.waypointsActual;

        if(posiciones[posicion] == null)
        {
            return;
        }

        //Calculamos del destino unos maximos y minimos para evitar que el enemigo no llegue a la zona por obstaculos
        float minX, maxX, minZ, maxZ;
        minX = posiciones[posicion].position.x - 3f;
        maxX = posiciones[posicion].position.x + 3f;
        minZ = posiciones[posicion].position.z - 3f;
        maxZ = posiciones[posicion].position.z + 3f;

        //comprobamos si llego al destino
        if (maxX >= my.transform.position.x
        && minX <= my.transform.position.x
        && maxZ >= my.transform.position.z
        && minZ <= my.transform.position.z)
        {
            //Ha llegado, cambiamos posicion
            posicion++;
            if (!(posicion < posiciones.Length))
            {
                //posicion no encontrada reiniciamos
                posicion = 0;
            }
            //Actualizamos posicion en el objeto
            parent.waypointsActual = posicion;
        }

        //actualizamos objetivo
        navmeshagent.destination = posiciones[posicion].position;
        navmeshagent.isStopped = false;
        navmeshagent.speed = 4.5f;

        //Ponemos animacion andar
        parent.GetComponent<Animator>().SetInteger("Estado", 0);
        parent.ponerNormal();

        //Cambiamos al siguiente estado ver
        cambiarVer();
    }
}