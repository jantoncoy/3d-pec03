﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EstadoPerseguir : Estado
{
    /// <summary>
    /// hace referencia al gameobject del enemigo
    /// </summary>
    private GameObject my;

    /// <summary>
    /// hace referencia al script enemigo del enemigo
    /// </summary>
    private Enemigo parent;

    /// <summary>
    /// hace referencia al componente navmeshagent del enemigo
    /// </summary>
    private NavMeshAgent navMeshAgent;

    /// <summary>
    /// Constructor del estado perseguir
    /// </summary>
    public EstadoPerseguir(GameObject my)
    {
        this.my = my;
        parent = my.GetComponent<Enemigo>();
        navMeshAgent = my.GetComponent<NavMeshAgent>();
    }

    /// <summary>
    /// cambia al estado ataque
    /// </summary>
    public void cambiarAtaque()
    {
        parent.estado = parent.estadoAtaque;
    }

    /// <summary>
    /// cambia al estado perseguir
    /// </summary>
    public void cambiarPerseguir()
    {
        Debug.Log("Ya se encuentra en el estado");
    }

    /// <summary>
    /// cambia al estado ver
    /// </summary>
    public void cambiarVer()
    {
        parent.estado = parent.estadoVer;
    }

    /// <summary>
    /// cambia al estado vigilar
    /// </summary>
    public void cambiarVigilar()
    {
        parent.estado = parent.estadoVigilar;
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerEnter(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerExit(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerStay(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// procesa el estado
    /// </summary>
    public void procesar()
    {
        float distancia = Vector3.Distance(parent.jugador.transform.position, my.transform.position);
        if (distancia > parent.distanciaPerseguir || parent.jugador.GetComponent<Jugador>().vida <= 0)
        {
            //Si esta muy lejos se pasa a vigilar
            parent.jugador = null;
            cambiarVigilar();
        } else if(distancia <= parent.distanciaAtaque)
        {
            cambiarAtaque();
        }
        else
        {
            my.GetComponent<Animator>().SetInteger("Estado", -1);
            parent.ponerNormal();
            navMeshAgent.destination = parent.jugador.transform.position;
            navMeshAgent.speed = 8;
            navMeshAgent.isStopped = false;
        }
    }
}