﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EstadoAtaque : Estado
{
    /// <summary>
    /// hace referencia al gameobject del enemigo
    /// </summary>
    private GameObject my;

    /// <summary>
    /// hace referencia al script enemigo del enemigo
    /// </summary>
    private Enemigo parent;

    /// <summary>
    /// hace referencia al componente navmeshagent del enemigo
    /// </summary>
    private NavMeshAgent navMeshAgent;

    /// <summary>
    /// Constructor del estado ataque
    /// </summary>
    public EstadoAtaque(GameObject my)
    {
        this.my = my;
        parent = my.GetComponent<Enemigo>();
        navMeshAgent = my.GetComponent<NavMeshAgent>();
    }

    /// <summary>
    /// cambia al estado ataque
    /// </summary>
    public void cambiarAtaque()
    {
        Debug.Log("Ya se encuentra en el estado");
    }

    /// <summary>
    /// cambia al estado perseguir
    /// </summary>
    public void cambiarPerseguir()
    {
        parent.estado = parent.estadoPerseguir;
    }

    /// <summary>
    /// cambia al estado ver
    /// </summary>
    public void cambiarVer()
    {
        parent.estado = parent.estadoVer;
    }

    /// <summary>
    /// cambia al estado vigilar
    /// </summary>
    public void cambiarVigilar()
    {
        parent.estado = parent.estadoVigilar;
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerEnter(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerExit(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerStay(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// procesa el estado
    /// </summary>
    public void procesar()
    {
        //Miramos al jugador
        parent.gameObject.transform.LookAt(parent.jugador.transform);
        
        //Activamos el arma
        if(parent.arma != null)
        {
            parent.arma.SetActive(true);

            Arma arma = parent.arma.GetComponent<Arma>();
            arma.disparar(parent.transform.rotation.eulerAngles, parent.gameObject);

            //Animacion de disparar
            my.GetComponent<Animator>().SetInteger("Estado", 1);
        }
        else
        {
            my.GetComponent<Animator>().SetInteger("Estado", 1);
        }

        parent.ponerAtaque();

        //Paramos al enemigo
        navMeshAgent.isStopped = true;
        
        //Cambiamos a perseguir
        cambiarPerseguir();
    }
}