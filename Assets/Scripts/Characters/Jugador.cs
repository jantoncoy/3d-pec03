using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Jugador : MonoBehaviour
{
    /// <summary>
    /// Vida restante
    /// </summary>
    public int vida = 0;
    
    /// <summary>
    /// Escudo restante
    /// </summary>
    public int escudo = 0;
    
    /// <summary>
    /// Municion del arma actual
    /// </summary>
    public int municion = 0;
    
    /// <summary>
    /// Array de armas
    /// </summary>
    public GameObject [] armas;

    /// <summary>
    /// Arma actual del jugador
    /// </summary>
    public GameObject arma;
    public int numArma;
    public bool sePuedeCambiarArma = true;
    public AudioSource sonidoEventos;

    /// <summary>
    /// Controlador del jugador
    /// </summary>
    private CharacterController controlador;

    /// <summary>
    /// Controlador de las animaciones del jugador
    /// </summary>
    private Animator animator;

    /// <summary>
    /// Gravedad
    /// </summary>
    public float gravedad = -9.8f;

    /// <summary>
    /// Velocidad
    /// </summary>
    public float velocidad = 5f;

    /// <summary>
    /// Velocidad
    /// </summary>
    public float velocidadSalto = 3f;

    /// <summary>
    /// Inputs
    /// </summary>
    private bool izquierda, derecha, arriba, abajo, dispara , saltar, cambiar, correr;

    /// <summary>
    /// Accionador a activar
    /// </summary>
    public Objeto objeto = null;

    // Start is called before the first frame update
    void Start()
    {
        controlador = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();

        sePuedeCambiarArma = true;
        this.numArma = -1;
        cambiarDeArma();//primera arma
    }

    /// <summary>
    /// Comprueba si existe un accionador
    /// </summary>
    private void comprobarAccionadores()
    {
        int objetos = LayerMask.NameToLayer("Objetos");
        int layerMask = (1 << objetos);

        //Trazamos tres rayos emulando la vista del tanque
        Vector3 vectorZonaCentral = this.transform.TransformDirection(Vector3.forward);

        //Almacenaremos las colisiones si existen
        RaycastHit hitCentral;

        //Levantamos el trazado
        Vector3 origen = new Vector3(this.transform.position.x, this.transform.position.y + 1f, this.transform.position.z);


        //Trazamos los rayos
        Physics.Raycast(origen,vectorZonaCentral, out hitCentral, 2f, layerMask);

        //Dibujamos trazado
        Debug.DrawRay(origen, vectorZonaCentral * 2f, Color.red);

        //Objeto
        Objeto resultado = null;

        //Comprobamos colision
        if (hitCentral.collider != null)
        {
            resultado = hitCentral.collider.gameObject.GetComponent<Objeto>();

            if (resultado.isAccionador)
            {
                if(!resultado.seAcciono)
                GestorUI.mostrarMensaje("<b>Activar</b> (disparar)",2f);
                else
                GestorUI.mostrarMensaje("<b>Desactivar</b> (disparar)", 2f);
            }
            else
            {
                resultado = null;
            }
        }

        this.objeto = resultado;
    }

    // Update is called once per frame
    void Update()
    {
        if(vida > 0)
        {
            comprobarInputs();
            procesarMovimiento();
            comprobarAccionadores();
        }

        //actualizamos vida y escudo
        GestorUI.actualizarEscudo(this.escudo);
        GestorUI.actualizarVida(this.vida);
    }

    /// <summary>
    /// Cambia el arma de un jugador
    /// </summary>
    public void cambiarDeArma()
    {
        if (sePuedeCambiarArma)
        {
            //Deshabilitamos el cambio de arma
            sePuedeCambiarArma = false;

            int numArma = this.numArma + 1;

            Debug.Log("Asigna arma");

            //Desactivamos el arma actual
            if (arma != null)
                arma.SetActive(false);

            //Asignamos el arma nueva
            if (numArma >= armas.Length)
            {
                //reiniciamos al primer arma
                numArma = 0;
            }

            arma = armas[numArma];
            arma.SetActive(true);
            this.numArma = numArma;

            if(GestorUI.actualizarArma != null)
            GestorUI.actualizarArma(this.numArma);

            //Cargamos la municion que tiene el arma
            municion = arma.GetComponent<Arma>().municion;
            sonidoEventos.Play();
        }

        Invoke("habilitarCambioArma", 1.4f);

    }

    /// <summary>
    /// Habilita el cambio de arma
    /// </summary>
    private void habilitarCambioArma()
    {
        sePuedeCambiarArma = true;
    }

    /// <summary>
    /// Llama a la accion de disparar
    /// </summary>
    public void disparar()
    {
        arma.GetComponent<Arma>().disparar(this.gameObject.transform.localRotation.eulerAngles, this.gameObject, this.objeto);
        if(objeto != null && !objeto.isFinalNivel)
            animator.SetInteger("Estado", 2);
        else if (objeto != null && objeto.isFinalNivel)
        {
            cursosYdeshabiltarMovimiento();
        }
        else
            animator.SetInteger("Estado", 1);
    }

    /// <summary>
    /// Procesa el movimiento segun los inputs
    /// </summary>
    private void procesarMovimiento()
    {
        //Saltar
        if (this.saltar)
        {
            animator.SetInteger("Estado", 4);
        }

        //Correr
        if (this.correr)
        {
            animator.SetInteger("Estado", 5);
        }

        //Disparamos
        if (dispara)
        {
            disparar();
        }

        //Cambiamos de arma
        if (cambiar)
        {
            cambiarDeArma();
        }
    }

    /// <summary>
    /// Procesa los inputs
    /// </summary>
    private void comprobarInputs()
    {
        //Se dehabilita control si esta muerto
        if(animator.GetInteger("Estado") == 3)
        {
            return;
        }
        else
        {
            //Volvemos al inicio
            animator.SetInteger("Estado", 0);
        }

        float horizontalInput = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float salto = Input.GetAxis("Jump");
        float dispara = Input.GetAxis("Fire1");
        float cambio = Input.GetAxis("Change");
        
        animator.SetFloat("VelX", horizontalInput);
        animator.SetFloat("VelY", vertical);

        if (horizontalInput > 0)
        {
            this.izquierda = false;
            this.derecha = true;
        }else if (horizontalInput < 0)
        {
            this.izquierda = true;
            this.derecha = false;
        }
        else
        {
            this.derecha = false;
            this.izquierda = false;
        }

        if (vertical > 0)
        {
            this.abajo = false;
            this.arriba = true;
        }
        else if (vertical < 0)
        {
            this.abajo = true;
            this.arriba = false;
        }
        else
        {
            this.abajo = false;
            this.arriba = false;
        }

        if (dispara > 0)
        {
            this.dispara = true;
        }
        else
        {
            this.dispara = false;
        }

        if (salto > 0)
        {
            this.saltar = true;
        }
        else
        {
            this.saltar = false;
        }

        if (cambio > 0)
        {
            this.cambiar = true;
        }
        else
        {
            this.cambiar = false;
        }

        correr = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
    }

    /// <summary>
    /// Detectamos las colisiones
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        Bala bala = collision.gameObject.GetComponent<Bala>();

        if (bala != null && bala.origin != this.gameObject)
        {
            quitarVida(bala.damage);
        }

        Debug.Log("Enemigo colisiona " + collision.gameObject.name);
    }

    /// <summary>
    /// Recogida de los eventos
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        Objeto objeto = other.gameObject.GetComponent<Objeto>();
        
        if (other.gameObject.name.Contains("GameOver"))
        {
            escudo = 0;
            vida = 0;
            GestorUI.activarPanelGameOver();
            cursosYdeshabiltarMovimiento();
        }

        if (objeto != null && objeto.isRecolectable)
        {
            int cantidad = objeto.cantidad;
            objeto.cantidad = 0;
            if (objeto.isVida)
            {
                this.sumarVida(cantidad);
            }
            else if (objeto.isMunicion)
            {
                this.sumaMunicion(cantidad);
            }
            else if (objeto.isEscudo)
            {
                this.sumarEscudo(cantidad);
            }
        }
    }

    /// <summary>
    /// Quita vida
    /// </summary>
    /// <param name="damage"></param>
    public void quitarVida(int damage)
    {
        int damageRestante = damage;

        if (escudo > 0)
        {
            int porcentajeEscudo = (int)((float)damage * 0.8f);
            int resultado = escudo - porcentajeEscudo;

            if (resultado < 0)
            {
                resultado = 0;
            }

            escudo = resultado;

            damageRestante = damage - porcentajeEscudo;
        }

        if(vida > 0)
        {
            int resultado = vida - damageRestante;

            if (resultado < 0)
            {
                resultado = 0;
            }

            vida = resultado;
        }

        if (vida <= 0)
        {
            //muere
            animator.SetInteger("Estado", 3);

            cursosYdeshabiltarMovimiento();

            //se quitan objetos
            arma.SetActive(false);

            //se muestra gameover
            GestorUI.activarPanelGameOver();
        }
    }

    /// <summary>
    /// Suma la vida a la actual
    /// </summary>
    /// <param name="vida"></param>
    private void sumarVida(int vida)
    {
        int resultado = this.vida + vida;

        if (resultado > 100)
        {
            resultado = 100;
        }

        this.vida = resultado;
    }

    /// <summary>
    /// Suma la municion al actual
    /// </summary>
    /// <param name="cantidad"></param>
    private void sumaMunicion(int cantidad)
    {
        this.arma.GetComponent<Arma>().municion += cantidad;
    }

    /// <summary>
    /// Suma la escudo al actual
    /// </summary>
    /// <param name="escudo"></param>
    private void sumarEscudo(int escudo)
    {
        int resultado = this.escudo + escudo;

        if(resultado > 100)
        {
            resultado = 100;
        }

        this.escudo = resultado;
    }

    /// <summary>
    /// Nos sirve para deshabilitar el movimiento y habilitar el cursor
    /// </summary>
    private void cursosYdeshabiltarMovimiento()
    {
        //Mostramos cursor y deshabilitamos el movimiento
        Destroy(this.gameObject.GetComponent<FirstPersonController>());
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
