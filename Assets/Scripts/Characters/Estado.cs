﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// interfaz para todas los estados de la IA
/// </summary>
public interface Estado
{
    void procesar();
    void cambiarAtaque();
    void cambiarPerseguir();
    void cambiarVigilar();
    void cambiarVer();

    void OnTriggerEnter(Collider col);
    void OnTriggerStay(Collider col);
    void OnTriggerExit(Collider col);

}