﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstadoVer : Estado
{
    /// <summary>
    /// hace referencia al gameobject del enemigo
    /// </summary>
    private GameObject my;

    /// <summary>
    /// hace referencia al script enemigo del enemigo
    /// </summary>
    private Enemigo parent;

    /// <summary>
    /// Constructor del estado ver
    /// </summary>
    public EstadoVer(GameObject my)
    {
        this.my = my;
        parent = my.GetComponent<Enemigo>();
    }

    /// <summary>
    /// cambia al estado ataque
    /// </summary>
    public void cambiarAtaque()
    {
        parent.estado = parent.estadoAtaque;
    }

    /// <summary>
    /// cambia al estado perseguir
    /// </summary>
    public void cambiarPerseguir()
    {
        parent.estado = parent.estadoPerseguir;
    }

    /// <summary>
    /// cambia al estado ver
    /// </summary>
    public void cambiarVer()
    {
        Debug.Log("Ya se encuentra en el estado");
    }

    /// <summary>
    /// cambia al estado vigilar
    /// </summary>
    public void cambiarVigilar()
    {
        parent.estado = parent.estadoVigilar;
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerEnter(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerExit(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// gestiona los triggers
    /// </summary>
    public void OnTriggerStay(Collider col)
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// procesa el estado
    /// </summary>
    public void procesar()
    {
        int jugadores = LayerMask.NameToLayer("Jugador");
        int layerMask = (1 << jugadores);

        //Trazamos tres rayos emulando la vista del tanque
        Vector3 vectorZonaMinima = Quaternion.AngleAxis(-15, Vector3.up) * my.transform.TransformDirection(Vector3.forward);
        Vector3 vectorZonaMaxima = Quaternion.AngleAxis(15, Vector3.up) * my.transform.TransformDirection(Vector3.forward);
        Vector3 vectorZonaCentral = my.transform.TransformDirection(Vector3.forward);

        //Almacenaremos las colisiones si existen
        RaycastHit hitIzquierda, hitCentral, hitDerecha;

        Vector3 origen = new Vector3(my.transform.position.x, parent.subirVista > 0 ? my.transform.position.y + parent.subirVista : my.transform.position.y, my.transform.position.z);

        //Trazamos los rayos
        Physics.Raycast(origen, vectorZonaMinima, out hitIzquierda, 20, layerMask);
        Physics.Raycast(origen, vectorZonaCentral, out hitCentral, 20, layerMask);
        Physics.Raycast(origen, vectorZonaMaxima, out hitDerecha, 20, layerMask);

        //Dibujamos en modo debug los rayos de vision
        Debug.DrawRay(origen, vectorZonaCentral * 20, Color.red);
        Debug.DrawRay(origen, vectorZonaMaxima * 20, Color.red);
        Debug.DrawRay(origen, vectorZonaMinima * 20, Color.red);


        //Comprobamos si existe colision
        GameObject jugador = comprobarColision(hitIzquierda, hitCentral, hitDerecha);

        //Si existe colision devolvemos true y informamos el vector del enemigo
        if (jugador != null)
        {
            //se ve jugador
            parent.jugador = jugador;

            if(Vector3.Distance(my.gameObject.transform.position,jugador.transform.position) > 15)
            {
                //lejos
                this.cambiarPerseguir();
            }
            else
            {
                //cerca
                this.cambiarAtaque();
            }
        }
        else
        {
            parent.jugador = null;
            //No se ve jugador
            this.cambiarVigilar();
        }
    }

    /// <summary>
    /// Comprueba si colisiono algun objeto y lo devuelve
    /// </summary>
    /// <param name="izq"></param>
    /// <param name="centro"></param>
    /// <param name="der"></param>
    /// <returns></returns>
    private GameObject comprobarColision(RaycastHit izq, RaycastHit centro, RaycastHit der)
    {
        GameObject resultado = null;

        if (centro.collider != null)
        {
            resultado = centro.collider.gameObject;
        }
        else if (izq.collider != null)
        {
            resultado = izq.collider.gameObject;
        }
        else if (der.collider != null)
        {
            resultado = der.collider.gameObject;
        }

        return resultado;
    }
}