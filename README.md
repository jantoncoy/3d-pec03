
# 3d Pec03

  

Juego de plataformas realizado para la PEC03 de 3D.

  

## Como jugar

  

### Para este juego tiene que utilizar los siguientes controles:

- **A o Joystick izquierdo hacia la izquierda** -> Ir hacia la izquierda

- **D o Joystick izquierdo hacia la derecha** -> Ir hacia la derecha

- **W o Joystick izquierdo hacia arriba** -> Ir hacia arriba

- **S o Joystick izquierdo hacia abajo** -> Ir hacia abajo

- **Space o Boton superior** -> Saltar

- **Q o boton izquierdo del raton o Boton inferior** -> Disparar o activar objeto

- **E o boton derecho del raton o Boton derecho** -> Cambiar de arma

- **Shift o Boton izquierdo** -> Correr
  

### Pasos para comenzar el juego:

  

1. Iniciar el ejecutable del juego.

  

2. Cuando se inicie el juego estara en la escena Menu donde tiene que pulsar encima del texto "Empezar" para empezar a jugar.

  

3. Debe de sobrevivir y llegar al helicoptero para escapar de la epidemia zombie.



## Video

  

[Enlace al video](https://youtu.be/n1JiEC1faWU "Enlace al video")

  

## Instaladores


- [ ] [Windows 10](https://drive.google.com/file/d/1e2Jys4uFQ6cKl202qV9UwA4SZM5RqrSH/view?usp=sharing)

- [ ] [Mac OS](https://drive.google.com/file/d/1QqI5khO31Xa2Oxw05P_YIEnS6O6Yjdxz/view?usp=sharing)


## Partes importantes del codigo

  **El proyecto esta basado en la PEC02, por lo que muchos puntos seran validos para esta PEC.**

#### Estructura del proyecto

  

- Assets

	- Scenes: En esta carpeta se encuentra las escenas del juego (Menu, Zona1_1).

	- Scripts: Se encuentra los scripts de todo el juego.

	- Images: Imagenes y sprites del juego.

	- Prefabs: Todos los prefabs del juego (protagonista, zombie, armas, items).

	- Materials: Materiales utilizados en el juego.

	- Models: Modelos y animaciones del juego.

	- Audios: Contiene los sonidos empleados en el juego.

#### Jugador

  

**Archivo: Assets/Scripts/Characters/Jugador.cs**

  

El jugador ejecuta en el update el procesamiento de los controles, como su movimiento y si detecta algun accionador. Ademas actualiza constantemente la vida y su escudo en el hud mediante eventos (Action).

En esta parte se ha agregado el control del salto y el modo de correr para mostrar las animaciones.
  
```csharp

    /// <summary>
    /// Procesa el movimiento segun los inputs
    /// </summary>
    private void procesarMovimiento()
    {
        //Saltar
        if (this.saltar)
        {
            animator.SetInteger("Estado", 4);
        }

        //Correr
        if (this.correr)
        {
            animator.SetInteger("Estado", 5);
        }

        //Disparamos
        if (dispara)
        {
            disparar();
        }

        //Cambiamos de arma
        if (cambiar)
        {
            cambiarDeArma();
        }
    }

```

```csharp

// Update is called once per frame

void  Update()

{

if(vida > 0)

{

comprobarInputs();

procesarMovimiento();

comprobarAccionadores();

}

  

//actualizamos vida y escudo

GestorUI.actualizarEscudo(this.escudo);

GestorUI.actualizarVida(this.vida);

}

```

 Para comprobar los accionadores o disparadores (activan algo) se realiza un trazado de rayos para ver si colisiona con algun componente de tipo objeto.

```csharp
private  void  comprobarAccionadores()

{

int  objetos = LayerMask.NameToLayer("Objetos");

int  layerMask = (1 << objetos);

  

//Trazamos tres rayos emulando la vista del tanque

Vector3  vectorZonaCentral = this.transform.TransformDirection(Vector3.forward);

  

//Almacenaremos las colisiones si existen

RaycastHit  hitCentral;

  

//Levantamos el trazado

Vector3  origen = new  Vector3(this.transform.position.x, this.transform.position.y + 1f, this.transform.position.z);

  
  

//Trazamos los rayos

Physics.Raycast(origen,vectorZonaCentral, out  hitCentral, 2f, layerMask);

  

//Dibujamos trazado

Debug.DrawRay(origen, vectorZonaCentral * 2f, Color.red);

  

//Objeto

Objeto  resultado = null;

  

//Comprobamos colision

if (hitCentral.collider != null)

{

resultado = hitCentral.collider.gameObject.GetComponent<Objeto>();

  

if (resultado.isAccionador)

{

if(!resultado.seAcciono)

GestorUI.mostrarMensaje("<b>Activar</b> (disparar)",2f);

else

GestorUI.mostrarMensaje("<b>Desactivar</b> (disparar)", 2f);

}

else

{

resultado = null;

}

}

  

this.objeto = resultado;

}
```
Detectamos mediante triggers y colisiones si al jugador le impacta un golpe. Ademas si detecta que tocas un item recogera la cantidad que proporcione el item.

```csharp
/// <summary>

/// Detectamos las colisiones

/// </summary>

/// <param  name="collision"></param>

private  void  OnCollisionEnter(Collision  collision)

{

Bala  bala = collision.gameObject.GetComponent<Bala>();

  

if (bala != null && bala.origin != this.gameObject)

{

quitarVida(bala.damage);

}

}

  

/// <summary>

/// Recogida de los eventos

/// </summary>

/// <param  name="other"></param>

private  void  OnTriggerEnter(Collider  other)

{

Objeto  objeto = other.gameObject.GetComponent<Objeto>();

if (other.gameObject.name.Contains("GameOver"))

{

escudo = 0;

vida = 0;

GestorUI.activarPanelGameOver();

cursosYdeshabiltarMovimiento();

}

  

if (objeto != null && objeto.isRecolectable)

{

int  cantidad = objeto.cantidad;

objeto.cantidad = 0;

if (objeto.isVida)

{

this.sumarVida(cantidad);

}

else  if (objeto.isMunicion)

{

this.sumaMunicion(cantidad);

}

else  if (objeto.isEscudo)

{

this.sumarEscudo(cantidad);

}

}

}
```

Para quitar vida al jugador siempre intentamos quitar vida del escudo un porcentaje del 80%, si no queda escudo se quita integramente de la vida. En el caso de que no le quede vida al final del calculo se procesara el Game Over.

```csharp
/// <summary>

/// Quita vida

/// </summary>

/// <param  name="damage"></param>

private  void  quitarVida(int  damage)

{

int  damageRestante = damage;

  

if (escudo > 0)

{

int  porcentajeEscudo = (int)((float)damage * 0.8f);

int  resultado = escudo - porcentajeEscudo;

  

if (resultado < 0)

{

resultado = 0;

}

  

escudo = resultado;

  

damageRestante = damage - porcentajeEscudo;

}

  

if(vida > 0)

{

int  resultado = vida - damageRestante;

  

if (resultado < 0)

{

resultado = 0;

}

  

vida = resultado;

}

  

if (vida <= 0)

{

//muere

animator.SetInteger("Estado", 3);

  

cursosYdeshabiltarMovimiento();

  

//se quitan objetos

arma.SetActive(false);

  

//se muestra gameover

GestorUI.activarPanelGameOver();

}

}
```

#### Objetos

**Archivo: Assets\Scripts\Objetos\Objeto.cs**

Los objetos tienen diferentes booleanos que indican que tipo de objeto es. En esta entrega solo hemos utilizado si es un objeto que hace que termine el nivel o un recolectable.

```csharp
private  void  OnTriggerStay(Collider  other)

{

//Si esta encima aplicamos el ultimo movimiento de la plataforma

if (isPlataforma && other.gameObject.CompareTag("Player") && ultimoMovimiento != 0)

{

CharacterController  personaje = other.gameObject.GetComponent<CharacterController>();

  

if (personaje != null)

personaje.Move(new  Vector3(0, 0, ultimoMovimiento));

  

ultimoMovimiento = 0;

}

}
```

#### Enemigos y IA

  

**Archivo: Assets\Scripts\Characters\Enemigo.cs**

Cuando inicializamos el enemigo, inicializamos todos los estados y lo dejamos en el estado ver (mira si ve al jugador).
  
```csharp

/// <summary>

/// Inicializamos variables

/// </summary>

void  Start()

{

this.estadoAtaque = new  EstadoAtaque(this.gameObject);

this.estadoPerseguir = new  EstadoPerseguir(this.gameObject);

this.estadoVer = new  EstadoVer(this.gameObject);

this.estadoVigilar = new  EstadoVigilar(this.gameObject);

  

estado = this.estadoVer;

navmeshagent = this.GetComponent<NavMeshAgent>();

animator = this.GetComponent<Animator>();

}
```
La IA esta limitada a varias ejecuciones por segundos en este caso 15 para no sobrecargar la ejecucion del juego.

  

```csharp
// Update is called once per frame

void  Update()

{

if (estaVivo)

{

if (acumulado > procesar)

{

//Procesamos si tiene vida

if (vida > 0)

estado.procesar();

  

acumulado = 0.0f;

}

else

{

//Sumamos tiempo

acumulado += Time.deltaTime / 0.001f;

}

  

if (!navmeshagent.isStopped)

{

//Ponemos animacion andar

animator.SetInteger("Estado", 0);

//desactivamos arma

arma.SetActive(false);

}

}

}
```

Tambien realizamos el Fade adquiriendo los materiales del modelo y aplicandoles transparencia.
  

```csharp
/// <summary>

/// desaparece al enemigo

/// </summary>

public  void  fade(float  transparencia)

{

if(this.modelo != null)

{

Material[] materiales = this.modelo.materials;

  

if (materiales != null && materiales.Length > 0)

foreach (Material  mat  in  materiales)

{

if (mat != null)

{

mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);

mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);

mat.SetInt("_ZWrite", 0);

mat.DisableKeyword("_ALPHATEST_ON");

mat.DisableKeyword("_ALPHABLEND_ON");

mat.EnableKeyword("_ALPHAPREMULTIPLY_ON");

mat.renderQueue = 3000;

mat.color = new  Color(mat.color.r, mat.color.g, mat.color.b, transparencia);

Debug.Log(transparencia + " " + mat.name);

}

}

}

  

}
```

Para activar los diferentes sonidos agregamos dos metodos para activarlos.

```csharp
    /// <summary>
    /// Pone el audio de ataque
    /// </summary>
    public void ponerAtaque()
    {
        if (audio != null && attack != null)
        {
            audio.clip = attack;
            audio.Play();
        }
            
    }

    /// <summary>
    /// Pone el audio de estado normal
    /// </summary>
    public void ponerNormal()
    {
        if (audio != null && normal != null && audio.clip != normal)
        {
            audio.clip = normal;
            audio.loop = true;
            audio.Play();
        }

    }
```

Cuando recibe daño el zombie activa su animacion de estar dañado, si existe otra animacion prioritaria omitira esta.

```csharp
    /// <summary>
    /// Cuando impacta un objeto salta este evento
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        Bala bala = collision.gameObject.GetComponent<Bala>();
        Jugador jugador = collision.gameObject.GetComponent<Jugador>();
        if (bala != null)
        {
            //Animacion dolor
            animator.SetInteger("Estado", 4);

            //quitan puntos de vida segun el damage interno
            if (bala.origin.GetComponent<Enemigo>() == null)
            {
                //Le quitamos vida
                quitarVida(bala.damage);

                //Le asignamos la posicion del jugador
                this.jugador = bala.origin;

                //Cambiamos a perseguir
                estado = estadoPerseguir;
            }
        }
        else if(jugador != null && estado.Equals(estadoAtaque))
        {
            //Hacemos daño
            jugador.quitarVida(ataqueCuerpo);
        }

        Debug.Log("Enemigo colisiona " + collision.gameObject.name);
    }
```

En el ultimo else se indica que si el enemigo impacta contra un jugador, le quite vida de esta forma recibe daño el jugador de un ataque cuerpo a cuerpo.

**IA: Atacar**
**Archivo: Assets\Scripts\Characters\EstadoAtaque**
 
En el caso de que se haya visto al jugador y este lo suficientemente cerca se procede a atacar para ello se gira el enemigo hacia el jugador, muestra el arma (si existe) y su respectiva animacion, realizando el ataque. En el caso que no tenga arma se realizara un ataque cuerpo a cuerpo. Al activar el ataque tambien activa el sonido de atacar.
 
 ```csharp
    /// <summary>
    /// procesa el estado
    /// </summary>
    public void procesar()
    {
        //Miramos al jugador
        parent.gameObject.transform.LookAt(parent.jugador.transform);
        
        //Activamos el arma
        if(parent.arma != null)
        {
            parent.arma.SetActive(true);

            Arma arma = parent.arma.GetComponent<Arma>();
            arma.disparar(parent.transform.rotation.eulerAngles, parent.gameObject);

            //Animacion de disparar
            my.GetComponent<Animator>().SetInteger("Estado", 1);
        }
        else
        {
            my.GetComponent<Animator>().SetInteger("Estado", 1);
        }

        parent.ponerAtaque();

        //Paramos al enemigo
        navMeshAgent.isStopped = true;
        
        //Cambiamos a perseguir
        cambiarPerseguir();
    }
```

**IA: Perseguir**
**Archivo: Assets\Scripts\Characters\EstadoPerseguir**

Si el jugador se encuentra a una distancia corta se procede a atacar, si se encuentra dentro de su distancia de reaccion seguira al jugador hasta que se encuentre cerca o lo suficientemente lejos para volver a realizar su vigilancia normal. La distancia para atacar se ha parametrizado y ahora se recoge de la clase enemigo. Ademas si persigue se pone el sonido normal del zombie y su animacion de perseguir.

```csharp
    /// <summary>
    /// procesa el estado
    /// </summary>
    public void procesar()
    {
        float distancia = Vector3.Distance(parent.jugador.transform.position, my.transform.position);
        if (distancia > parent.distanciaPerseguir || parent.jugador.GetComponent<Jugador>().vida <= 0)
        {
            //Si esta muy lejos se pasa a vigilar
            parent.jugador = null;
            cambiarVigilar();
        } else if(distancia <= parent.distanciaAtaque)
        {
            cambiarAtaque();
        }
        else
        {
            my.GetComponent<Animator>().SetInteger("Estado", -1);
            parent.ponerNormal();
            navMeshAgent.destination = parent.jugador.transform.position;
            navMeshAgent.speed = 8;
            navMeshAgent.isStopped = false;
        }
    }
```

**IA: Ver**
**Archivo: Assets\Scripts\Characters\EstadoVer**

Se encarga de generar tres rayos en forma de triangulo que representan su vision, si uno de ellos colisiona con el enemigo pasara a perseguir y si esta lo suficientemente cerca a atacar.

```csharp
public  void  procesar()

{

int  jugadores = LayerMask.NameToLayer("Jugador");

int  layerMask = (1 << jugadores);

  

//Trazamos tres rayos emulando la vista del tanque

Vector3  vectorZonaMinima = Quaternion.AngleAxis(-15, Vector3.up) * my.transform.TransformDirection(Vector3.forward);

Vector3  vectorZonaMaxima = Quaternion.AngleAxis(15, Vector3.up) * my.transform.TransformDirection(Vector3.forward);

Vector3  vectorZonaCentral = my.transform.TransformDirection(Vector3.forward);

  

//Almacenaremos las colisiones si existen

RaycastHit  hitIzquierda, hitCentral, hitDerecha;

  

Vector3  origen = new  Vector3(my.transform.position.x, parent.subirVista > 0 ? my.transform.position.y + parent.subirVista : my.transform.position.y, my.transform.position.z);

  

//Trazamos los rayos

Physics.Raycast(origen, vectorZonaMinima, out  hitIzquierda, 20, layerMask);

Physics.Raycast(origen, vectorZonaCentral, out  hitCentral, 20, layerMask);

Physics.Raycast(origen, vectorZonaMaxima, out  hitDerecha, 20, layerMask);

  

//Dibujamos en modo debug los rayos de vision

Debug.DrawRay(origen, vectorZonaCentral * 20, Color.red);

Debug.DrawRay(origen, vectorZonaMaxima * 20, Color.red);

Debug.DrawRay(origen, vectorZonaMinima * 20, Color.red);

  
  

//Comprobamos si existe colision

GameObject  jugador = comprobarColision(hitIzquierda, hitCentral, hitDerecha);

  

//Si existe colision devolvemos true y informamos el vector del enemigo

if (jugador != null)

{

//se ve jugador

parent.jugador = jugador;

  

if(Vector3.Distance(my.gameObject.transform.position,jugador.transform.position) > 15)

{

//lejos

this.cambiarPerseguir();

}

else

{

//cerca

this.cambiarAtaque();

}

}

else

{

parent.jugador = null;

//No se ve jugador

this.cambiarVigilar();

}

}
```

**IA: Vigilar**
**Archivo: Assets\Scripts\Characters\EstadoVigilar**

Se encarga de moverse entre sus diferentes waypoints, ahora tambien controla errores de nulos, aplica el sonido y animacion de andar.

```csharp
        if(parent.waypoints == null || parent.waypoints.Count == 0)
        {
            return;
        }

        //se inicializa al primero waypoint
        if(parent.waypointsActual < 0)
        {
            parent.waypointsActual = 0;
        }

        //Obtenemos todas las posiciones
        Transform[] posiciones = parent.waypoints.ToArray();
        int posicion = parent.waypointsActual;

        if(posiciones[posicion] == null)
        {
            return;
        }

        //Calculamos del destino unos maximos y minimos para evitar que el enemigo no llegue a la zona por obstaculos
        float minX, maxX, minZ, maxZ;
        minX = posiciones[posicion].position.x - 3f;
        maxX = posiciones[posicion].position.x + 3f;
        minZ = posiciones[posicion].position.z - 3f;
        maxZ = posiciones[posicion].position.z + 3f;

        //comprobamos si llego al destino
        if (maxX >= my.transform.position.x
        && minX <= my.transform.position.x
        && maxZ >= my.transform.position.z
        && minZ <= my.transform.position.z)
        {
            //Ha llegado, cambiamos posicion
            posicion++;
            if (!(posicion < posiciones.Length))
            {
                //posicion no encontrada reiniciamos
                posicion = 0;
            }
            //Actualizamos posicion en el objeto
            parent.waypointsActual = posicion;
        }

        //actualizamos objetivo
        navmeshagent.destination = posiciones[posicion].position;
        navmeshagent.isStopped = false;
        navmeshagent.speed = 4.5f;

        //Ponemos animacion andar
        parent.GetComponent<Animator>().SetInteger("Estado", 0);
        parent.ponerNormal();

        //Cambiamos al siguiente estado ver
        cambiarVer();
```

#### HUD y UI

  

**Archivo: Assets\Scripts\UI\GestorUI.cs**

En el GestorUI lo que hacemos es gestionar los indicadores del hud del jugador, asi como los eventos de derrota, victoria y cargar una zona o nivel. 

Para mostrar cada uno de los paneles le agregamos la funcionalidad de activarlos como un evento Action desde cualquier parte del codigo.  

```csharp
// Start is called before the first frame update

void  Start()

{

GestorUI.actualizarArma = updateArma;

GestorUI.actualizarMunicion = updateMunicion;

GestorUI.actualizarVida = updateVida;

GestorUI.actualizarEscudo = updateEscudo;

GestorUI.mostrarMensaje = mostrarMensajeEmergente;

  

GestorUI.activarPanelCargando = (string  nombreScene) => {

this.panelCargando.SetActive(true);

SceneManager.LoadScene(nombreScene);

};

  

GestorUI.activarPanelVictoria = () => {

this.panelVictoria.SetActive(true);

};

  

GestorUI.activarPanelGameOver = () => {

this.panelGameOver.SetActive(true);

};

}
```
Cambiamos tambien los iconos del arma cuando se cambia.

```csharp
/// <summary>

/// Cambia el icono del arma

/// </summary>

/// <param  name="value"></param>

private  void  updateArma(int  value)

{

if(value == 0)

{

//Arma principal potente

arma1.SetActive(true);

arma2.SetActive(false);

}

else  if(value == 1)

{

//Arma secundaria ligera

arma1.SetActive(false);

arma2.SetActive(true);

}

}
```

Ademas se implementa un mensaje emergente para indicar ayudas o cosas puntuales (como es la forma de activar un activador).

```csharp
/// <summary>

/// Muestra un mensaje durante el tiempo pasado por parametro

/// </summary>

/// <param  name="mensaje"></param>

/// <param  name="tiempoSegundos"></param>

private  void  mostrarMensajeEmergente(String  mensaje, float  tiempoSegundos)

{

CancelInvoke("ocultarMensaje");

mensajeObject.SetActive(true);

this.mensaje.text = mensaje;

Invoke("ocultarMensaje", tiempoSegundos);

}
```
#### Spameador o creador de entidades

Los objetos que crean entidades de forma aleatoria se incluyen en un objeto que hara de inicio para las entidades, se le asigna una entidad al creador de entidades y se le indica cada cuanto tiempo tiene que aumentar el numero de entidades y cual sera el numero maximo de entidades que genera a la vez.

  **Archivo: Assets\Scripts\Objetos\Spameador.cs**
  
  ```csharp
  /// <summary>
/// Clase que crear instancias de un objeto pasado, conforme pasa el tiempo genera hasta el maximo que se le indique
/// </summary>
public class Spameador : MonoBehaviour
{
    /// <summary>
    /// Entidades que genera inicialmente
    /// </summary>
    public int cantidadInicial = 1;

    /// <summary>
    /// Entidades maximos que genera
    /// </summary>
    public int cantidadMaximo = 10;

    /// <summary>
    /// Cada tiempoIncremento aumenta la cantidadInicial hasta cantidadMaxima(en segundos)
    /// </summary>
    public int tiempoIncremento = 30;

    /// <summary>
    /// Cada tiempoSpameo crear las entidades (en segundos)
    /// </summary>
    public int tiempoSpameo = 20;

    /// <summary>
    /// Indica si se enecuentra activo la creacion de entidades
    /// </summary>
    public bool activo = true;

    /// <summary>
    /// Entidad que genera
    /// </summary>
    public GameObject entidad;

    /// <summary>
    /// Lista de puntos que recorrera la entidad
    /// </summary>
    public List<Transform> waypoints;

    /// <summary>
    /// Inicializa los arrays
    /// </summary>
    private void Start()
    {
        InvokeRepeating(nameof(crearMonstruos), 1, tiempoSpameo);
        InvokeRepeating(nameof(aumentarCantidad), tiempoIncremento, tiempoIncremento);
    }

    /// <summary>
    /// Aumenta la cantidad inicial en un 20% de la cantidad maxima
    /// </summary>
    private void aumentarCantidad()
    {
        if (activo)
        {
            int nuevaCantidad = (int)(cantidadInicial + ((float)cantidadMaximo * 0.2f));

            if(nuevaCantidad > cantidadMaximo)
            {
                cantidadInicial = cantidadMaximo;
            }
            else
            {
                cantidadInicial = nuevaCantidad;
            }
        }
    }

    /// <summary>
    /// Crea tantos monstruos como la cantidad inicial
    /// </summary>
    private void crearMonstruos()
    {
        if (activo)
        {
            GameObject nuevaEntidad = Instantiate(entidad, this.transform.position, this.transform.rotation);
            
            Enemigo enemigo = entidad.GetComponent<Enemigo>();
            
            if(enemigo != null)
            enemigo.waypoints = this.waypoints;
        }
    }
}
  ```

#### Armas

  **Archivo: Assets\Scripts\Armas\Arma.cs**

En este script controlamos la creacion de la bala con sus propiedades (velocidad, daño y origen), ademas controlamos la municion y tiempo de recarga de la bala.

```csharp
public  void  disparar(Vector3  rotaciones, GameObject  origen, Objeto  objeto = null)

{

if (sePuedeDisparar && objeto != null && objeto.isAccionador)

{

this.sePuedeDisparar = false;

objeto.quienLoAcciona = origen;

objeto.realizarAccion();

Invoke("reiniciarDisparo", 1.4f);

}

else  if (sePuedeDisparar && municion > 0)

{

this.sePuedeDisparar = false;

Vector2  rotaciones2 = this.prefabBala.transform.rotation.eulerAngles;

Quaternion  rotacion = Quaternion.Euler(new  Vector3(rotaciones2.x,rotaciones.y, rotaciones.z));

Vector3  posicion = this.centroTiro.transform.position;

GameObject  bala = Instantiate(prefabBala, posicion, rotacion);

bala.GetComponent<Bala>().velocidad = this.velocidad;

bala.GetComponent<Bala>().damage = this.damage;

bala.GetComponent<Bala>().origin = origen;

  

Invoke("reiniciarDisparo", tiempoRecarga);

audioSource.Play();

if(!isEnemigo)

municion--;

}

}

// Update is called once per frame

void  Update()

{

if(!isEnemigo)

GestorUI.actualizarMunicion(municion);

}
```

 **Archivo: Assets\Scripts\Armas\Bala.cs**

En la bala cuando se inicializa se le indica un tiempo de vida fijo para que la bala aunque no colisione con nada se destruya al rato (evitamos sobrecargas), por otra parte controlamos la colision y avance de la misma como se puede ver en el codigo.

```csharp

/// <summary>

/// Gestiona cuando la bala colisiona

/// </summary>

private  void  OnCollisionEnter(Collision  collision)

{

if (!collision.gameObject.name.Contains("Bala")

&& !collision.gameObject.name.Contains("Arma")

&& collision.gameObject != origin)

{

colisiona();

}

}

  

/// <summary>

/// Indica que hacer cuando la bala colisiona

/// </summary>

private  void  colisiona()

{

colisiono = true;

//Se pone velocidad a cero

this.rigid.velocity = Vector3.zero;

Debug.Log("Colisiona la bala");

//Se para audio

audioSource.Stop();

//Se agrega audio de explosion o impacto

audioSource.clip = explosion;

//Se reproduce sonido

audioSource.Play();

}

// Start is called before the first frame update

void  Start()

{

colisiono = false;

rigid = this.GetComponent<Rigidbody>();

  

//Pasado el tiempo de la bala se destruye

Invoke("destruirBala", tiempoDeVida);

}

/// <summary>

/// Avanza la bala

/// </summary>

private  void  avanzar()

{

Vector3  velocidad = this.gameObject.transform.up * this.velocidad;

this.rigid.velocity = velocidad;

}



```

## Creditos

  

Creditos de las diferentes obras que se han utilizado:

  

### Sonidos

  

- Musica del menu

Autor: **Augmentality**

Enlace: OpenGameArts

- Musica del nivel

Autor: **Augmentality**

Enlace: OpenGameArts

- Sonidos Armas (disparo, impacto)

Autor: **Q009**

Enlace: OpenGameArts

- Sonidos zombies

Autor: **Darsycho**

Enlace: OpenGameArts


### Tiles, Imagenes

- Imagen menu

Autor: **William.Thompsonj**

Enlace: OpenGameArts

- Sprite barra de vida, escudo

Autor: **Raincode**

Enlace: OpenGameArts

### Modelos

- Helicoptero

Autor: **Brylie**

Enlace: OpenGameArts

- Items

Autor: **OwlishMedia**

Enlace: OpenGameArts

- Personajes y animaciones

[Los modelos del jugador y enemigos, ademas de las animaciones han sido sacados de Mixamo plataforma perteneciente adobe pero de uso gratuito.](https://helpx.adobe.com/la/creative-cloud/faq/mixamo-faq.html)